(require 'package)

(add-to-list 'package-archives '("org" . "http://orgmode.org/elpa/"))
(add-to-list 'package-archives '("melpa" . "http://melpa.org/packages/"))
(add-to-list 'package-archives '("melpa-stable" . "http://stable.melpa.org/packages/"))

(setq package-enable-at-startup nil)
(package-initialize)

; use-package
(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package))

(eval-when-compile
  (require 'use-package))

(use-package evil
  :ensure t
  :config (evil-mode t))

(use-package atom-one-dark-theme
  :ensure t)

(use-package helm
  :ensure t
  :config (helm-mode t))

(use-package company
  :ensure t
  :config (company-mode t))
(add-hook 'after-init-hook 'global-company-mode)

(use-package cider
  :ensure t)

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(initial-buffer-choice "~/")
 '(package-selected-packages (quote (atom-one-dark-theme helm evil use-package))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )

(tool-bar-mode -1)

(scroll-bar-mode -1)

(menu-bar-mode -1)

(fringe-mode 4)

(set-face-background 'fringe "inherit")

(add-to-list 'default-frame-alist '(fullscreen . maximized))

;; File type specific settings
;; CSS
(setq css-indent-offset 2)
