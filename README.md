# Install emacs (if not already installed)
    
    sudo apt-get-repository ppa:kelleyk/emacs

    sudo apt-get update

    sudo apt-get install emacs26

# Download this config
    
    git clone https://gitlab.com/dylandrake/dot-emacs
    
# Remove old emacs config (if one exists)
    
    rm -r .emacs.d/

# Use this config
     
    mv dot-emacs .emacs.d

    emacs26

Packages should install automatically once emacs starts
